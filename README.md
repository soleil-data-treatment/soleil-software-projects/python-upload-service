# python-upload-service

A python CGI that works on top of Apache in order to upload annotated data sets for AI training.

:warning: This project is OBSOLETE. Please use to : https://gitlab.com/soleil-data-treatment/soleil-software-projects/data-upload-service

## Installation

First install the required libraries
```
sudo apt install apache2 python3-ldap
sudo a2enmod cgid
sudo systemctl restart apache2
```
Then install the service itself:
- Tune the `src/file_upload.py` configuration section. For instance the storage location is set to `data/ai`. this means that the `/var/www/html/data/ai` should be created.
- Place the `src/file_upload.html` page for instance in `/var/www/html`.
- Place the `src/file_upload.py` in `/usr/lib/cgi-bin`.


## Usage

Open a browser at:
- http://localhost/file_upload.html
