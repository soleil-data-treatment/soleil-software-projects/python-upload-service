#!/usr/bin/python3

import cgi, sys, os
import traceback
import shutil
from datetime import datetime
import cgitb  # activate error reporting

# service configuration --------------------------------------------------------
www_root = '/var/www/html/'
target   = 'data/ai/'    # where to store the transfered files, e.g. '/tmp/'
ldap_uri = None           # e.g. when user id must be checked

# we access the form content ---------------------------------------------------
form = cgi.FieldStorage()

# test for form consistency
fields   = [ 'user_id','dataset_name','dataset_origin','dataset_annotation','dataset_description','dataset' ]
error    = None
message  = ""
dataset_file_nb = 0

print("Content-type: text/html\n")       # wrap up in html
print("<TITLE>SOLEIL/GRADES: AI Dataset Upload</TITLE>")

# we catch error
try:
    for f in fields:
        if not error and f not in form:
            raise Exception(f'Incomplete form: fields are missing: {f}')

    if not error and ldap_uri and 'user_pw' not in form:
        raise Exception('Incomplete form: fields are missing.')
        
    # test if required information is there
    if not len(form['user_id'].value) or not len(form['dataset_name'].value):
        raise Exception('Some required (*) information is missing.')
        
    # get a valid dataset_name (will be a directory)
    dataset_name = "".join( x for x in form['dataset_name'].value if (x.isalnum() or x in "._-")) # validate

    # scan files to upload
    datasets = form['dataset']

    # make it a list when given as single
    if not isinstance(datasets, list):
        datasets = [datasets]
        
    print(f'<H1>Data Upload: {dataset_name}: {len(datasets)} file(s)</H1>\n')

    # transfer all given files -----------------------------------------------------
    print('<ol>')
    for dataset in datasets:
        if dataset.filename:
        
            # create target directory if it does not exist yet
            target_dataset_dir = os.path.join(www_root, target, dataset_name)
            if not os.path.isdir(target_dataset_dir):
                os.makedirs(target_dataset_dir)
            
            # save file
            fn = "".join( x for x in dataset.filename if (x.isalnum() or x in "._-")) # make it a valid file name
            target_dataset = os.path.join(target_dataset_dir, fn)
            with open(target_dataset, 'wb') as f:
                shutil.copyfileobj(dataset.file, f)
            dataset_file_nb += 1
            print('<li>%s</li>\n' % dataset.filename)
            
    # end for dataset
    print('</ol>')

    # generate the text to display/write
    if dataset_file_nb:
        # format the YAML description
        url = os.path.join(os.environ['HTTP_ORIGIN'], target, dataset_name)
        print(f'<h2>The data has been uploaded in <a href="{url}">{url}</a></h2>')
        dat = datetime.now()
        yaml = f"""Location: {url}
Upload_date: {dat}
"""
        for f in fields:
            if f != 'dataset' and len(form[f].value):
                yaml += f + ': ' + form[f].value + '\n'
        
        # write the YAML output file
        target_dataset_txt = os.path.join(www_root, target, dataset_name, 'README.html')
        with open(target_dataset_txt, 'w') as f:
            f.write(f'<h1>Dataset {dataset_name}</h1>')
            f.write(f'Stored in <a href="{url}">{url}</a><br>')
            f.write('<pre>\n')
            f.write(yaml)
            f.write('</pre>\n')
            
        
        # print the output
        print('<HR><PRE>')
        print(yaml)
        print('</PRE><HR>')
        
        url = os.path.join(os.environ['HTTP_ORIGIN'], target)
        print(f'All data sets are available in <a href="{url}">{url}</a>')
    else:
        raise('Nothing to upload (no data set is specified).')
        
except Exception as e:
    print('<h1>ERROR uploading data set</h1>\n')
    print('<h2>\n')
    print(e)
    print('</h2>\n')
    
# end of file ------------------------------------------------------------------
